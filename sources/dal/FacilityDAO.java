package dal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import model.facility.Facility;
import model.facility.Facility_Detail;
import model.facility.IFacility;
import model.maintenance.Maintenance;
import model.maintenance.Maintenance_Request;

public class FacilityDAO {

	public FacilityDAO(){}
	//TODO Remove double slash on line 13 when Facility --> getFacility is implemented
	public  Facility getFacility(int number){

		try{
			Statement st = DBHelper.getConnection().createStatement();
			String selectFacilityQuery = "SELECT number, capacity, detail FROM Facility WHERE number = '" + number + "'"; 
			ResultSet factrs = st.executeQuery(selectFacilityQuery);      
			System.out.println("FacilityDAO: *************** Query " + selectFacilityQuery);

			//Get Facility
			Facility host = new Facility();
			while(factrs.next())
			{
				host.setNumber(factrs.getInt("number"));
				host.setCapacity(factrs.getInt("capacity"));
				host.setDetail((Facility_Detail) factrs.getObject("detail"));

			}
			//close to manage resources
			factrs.close();
			/*//TODO does Maintenance and Facility go hand-in-hand 
			//Get Maintenance
			String selectFacilityMaintenanceQuery = "SELECT workBoard, facilityNumber FROM Maintenance WHERE facility_Number = '" + number + "'";
			ResultSet maintrs = st.getResultSet();

			Maintenance oatmeal = new Maintenance(); 

			System.out.println("FacilityDAO: *************** Query " + selectFacilityMaintenanceQuery);
			while(maintrs.next()){

			}

			//host.setFacilityMaintenance(upkeep);
			//close to manage resources


			//return host;
			 */
			st.close();
			return host;
		}
		catch(SQLException se){
			System.out.println("DBHelper: Threw an SQL Exception trying to get a Facility");
			System.out.println(se.getMessage());
			se.printStackTrace();

		}
		return null;
	}
	@SuppressWarnings("unchecked")
	public Maintenance getMaintenance(int number){
		try{
			Statement st = DBHelper.getConnection().createStatement();
			String selectFacilityMaintenanceQuery = "SELECT workBoard, facilityNumber FROM Maintenance WHERE facility_Number = '" + number + "'";
			ResultSet factrs = st.executeQuery(selectFacilityMaintenanceQuery);      
			System.out.println("FacilityDAO: *************** Query " + selectFacilityMaintenanceQuery);

			//Get Maintenance
			Maintenance host = new Maintenance();
			while(factrs.next())
			{
				host.setFacilityNumber(factrs.getInt("facilityNumber"));
				host.setWorkBoard((ArrayList<Maintenance_Request>) factrs.getObject("workBoard"));

			}
			//close to manage resources
			factrs.close();
			st.close();
		}

		//return host;
		catch(SQLException se){
			System.out.println("DBHelper: Threw an SQL Exception trying to get a Maintenance");
			System.out.println(se.getMessage());
			se.printStackTrace();
		}
		return null;

	}
	public ArrayList<Facility> getAllFacility(){
		try{
			Statement st = DBHelper.getConnection().createStatement();
			String selectAllFacilityQuery = "SELECT * FROM Facility"; 
			ResultSet factrs = st.executeQuery(selectAllFacilityQuery);      
			System.out.println("FacilityDAO: *************** Query " + selectAllFacilityQuery);

			//Get Facility
			ArrayList<Facility> al = new ArrayList<Facility>();
			Facility host = new Facility();
			while(factrs.next())
			{
				host.setNumber(factrs.getInt("number"));
				host.setCapacity(factrs.getInt("capacity"));
				host.setDetail((Facility_Detail) factrs.getObject("detail"));			

				al.add(host);
			}
			//close to manage resources
			factrs.close();
			return al; 
			
		}
		catch(SQLException se){
			System.out.println("DBHelper: Threw an SQL Exception trying to get a Facility");
			System.out.println(se.getMessage());
			se.printStackTrace();

		}
		return null;	
	}
	
	//TODO think about adding IFacility as an interface , not a concrete class 
	public void addFacility(IFacility faci){
		Connection con = DBHelper.getConnection();
		PreparedStatement factPst = null;
		PreparedStatement addPst = null; 

		try{
			//Insert the Facility object
			String factStm = "INSERT INTO Facility(number, capacity, detail) values(?, ?, ?)";
			factPst = con.prepareStatement(factStm);
			factPst.setInt(1, faci.getNumber());
			factPst.setInt(2, faci.getCapacity());
			factPst.setObject(3, faci.getDetail());
			factPst.executeUpdate();

			/*//Insert the FacilityMaintenance object
			String addStm = "INSERT INTO FacilityMaintenance(x,y,z) values(?, ?, ?)"; 
			addPst = con.prepareStatement(addStm);
			//addPst.setString(1, faci.getFacilityInformation().getX());
			addPst.executeUpdate();
			 */
		}
		catch(SQLException se){
			System.out.println("DBHelper: Error inserting Facility object");
			System.out.println(se.getMessage());
			se.printStackTrace();
		}
		finally{
			try{
				if(addPst != null)
				{
					factPst.close();
					addPst.close();
				}
				if(con != null)
					con.close();
			}
			catch(SQLException ex){
				System.err.println("DBHelper: Threw a SQLException saving the facility object.");
				System.err.println(ex.getMessage());
			}
		}		
	}
	public void addMaintenance(Maintenance main){
		Connection con = DBHelper.getConnection();
		PreparedStatement factPst = null;
		PreparedStatement addPst = null; 

		try{
			//Insert the Maintenance object
			String factStm = "INSERT INTO Facility(number, capacity, detail) values(?, ?, ?)";
			factPst = con.prepareStatement(factStm);
			factPst.setObject(1, main.getWorkBoard());
			factPst.setInt(2, main.getFacilityNumber());
			factPst.executeUpdate();

		}
		catch(SQLException se){
			System.out.println("DBHelper: Error inserting Maintenance object");
			System.out.println(se.getMessage());
			se.printStackTrace();
		}
		finally{
			try{
				if(addPst != null)
				{
					factPst.close();
					addPst.close();
				}
				if(con != null)
					con.close();
			}
			catch(SQLException ex){
				System.err.println("DBHelper: Threw a SQLException saving the maintenance object.");
				System.err.println(ex.getMessage());
			}
		}		
	}
}
