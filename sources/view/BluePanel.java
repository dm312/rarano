package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class BluePanel extends JPanel{

	public BluePanel(){
		BorderLayout bl = new BorderLayout();
		Color blue = new Color(32,32,64);
		this.setBackground(blue);
		this.setLayout(bl);

		JPanel buttonsPanel = new JPanel();
		FlowLayout fl = new FlowLayout();
		buttonsPanel.setLayout(fl);
		buttonsPanel.setBackground(blue);

		JButton listFacilities = new JButton("List Facilities");
		buttonsPanel.add(listFacilities);
		JButton getFacilityInformation = new JButton("Get Facility Information");
		buttonsPanel.add(getFacilityInformation);
		JButton addNewFacility = new JButton("Add new Facility");
		buttonsPanel.add(addNewFacility);
		JButton addFacilityDetail= new JButton("Add Facility Detail");
		buttonsPanel.add(addFacilityDetail);
		JButton removeFacility = new JButton("Remove Facility");
		buttonsPanel.add(removeFacility);

		this.add(buttonsPanel, BorderLayout.CENTER);
	}

}
