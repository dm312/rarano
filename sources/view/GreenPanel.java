package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class GreenPanel extends JPanel{

	public GreenPanel(){
		BorderLayout bl = new BorderLayout();
		this.setLayout(bl);
		JPanel buttonsPanel = new JPanel();
		Color green = new Color(64,64,32);
		buttonsPanel.setBackground(green);
		
		JButton isInUseDuringInterval = new JButton("Check in use");
		buttonsPanel.add(isInUseDuringInterval);
		JButton assignFacilityToUse= new JButton("Assign facility to use");
		buttonsPanel.add(assignFacilityToUse);
		JButton vacateFacility= new JButton("Vacate Facility");
		buttonsPanel.add(vacateFacility);
		JButton listInspections = new JButton("List inspections");
		buttonsPanel.add(listInspections);
		JButton listActualUsage= new JButton("List actual usage");
		buttonsPanel.add(listActualUsage);
		JButton calcUsageRate = new JButton("Calc usage rate");
		buttonsPanel.add(calcUsageRate);
		
		FlowLayout fl = new FlowLayout();
		buttonsPanel.setLayout(fl);
		this.add(buttonsPanel, BorderLayout.CENTER);
	}

}
