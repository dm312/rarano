package view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Color;

import javax.swing.JButton;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class RedPanel extends JPanel{

	public RedPanel(){
		BorderLayout bl = new BorderLayout();
		this.setLayout(bl);
		JPanel buttonsPanel = new JPanel();
		FlowLayout fl = new FlowLayout();
		buttonsPanel.setLayout(fl);
		Color red = new Color(64,44,32);
		buttonsPanel.setBackground(red);

		
		JButton makeFacilityMaintRequest = new JButton("Make Facility Maint Request");
		buttonsPanel.add(makeFacilityMaintRequest);
		JButton scheduleMaintenance = new JButton("Schedule Maintenance");
		buttonsPanel.add(scheduleMaintenance);
		JButton calcMaintenanceCostForFacility = new JButton("Calc Maintenance Cost for Facility");
		buttonsPanel.add(calcMaintenanceCostForFacility);
		JButton calcProblemRateForFacility = new JButton("Calc Problem Rate for Facility");
		buttonsPanel.add(calcProblemRateForFacility);
		JButton calcDownTimeForFacility = new JButton("Calc Down Time for Facility");
		buttonsPanel.add(calcDownTimeForFacility);
		JButton listMaintRequests = new JButton("List Maint Requests");
		buttonsPanel.add(listMaintRequests);
		JButton listMaintenance = new JButton("List Maintenance");
		buttonsPanel.add(listMaintenance);
		JButton listFacilityProblems = new JButton("List Facility Problems");
		buttonsPanel.add(listFacilityProblems);
		
		this.add(buttonsPanel, BorderLayout.CENTER);
	}

}
