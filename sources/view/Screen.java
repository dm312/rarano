package view;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.*;

public class Screen {

	public static void main(String[] args){
            System.out.println("Hello, world");
		SwingUtilities.invokeLater(new Runnable(){
			public void run(){
				createAndBuildGUI();
			}
		});
	}

	public static void createAndBuildGUI()
	{
		JFrame frame = new JFrame();
		JTabbedPane bar = new JTabbedPane(3); 
		
		BluePanel Facility = new BluePanel();
		GreenPanel Use = new GreenPanel();
		RedPanel Maintenance = new RedPanel();
		
		bar.add("Facility", Facility);
		bar.add("Use", Use);
		bar.add("Maintenance", Maintenance);
		
		bar.setTabPlacement(JTabbedPane.TOP);
		
		frame.add(bar);
		
		frame.setSize(new Dimension(800,400));
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		
	}
}
