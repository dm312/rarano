package model.maintenance;

import java.sql.Date;

public class Maintenance_Order implements IMaintenanceOrder {

	private int id;

	private double cost;

	private Date repairStart;
	private Date repairEnd;

	/*
	public MaintenanceOrder(int id, double cost, Date repairStart, Date repairEnd) {
		this.id = id;
		this.cost = cost;
		this.repairStart = repairStart;
		this.repairEnd = repairEnd;
	}

	public MaintenanceOrder(double cost, Date repairStart, Date repairEnd) {
		this.cost = cost;
		this.repairStart = repairStart;
		this.repairEnd = repairEnd;
	}

	 */
	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public double getCost() {
		return cost;
	}


	public void setCost(double cost) {
		this.cost = cost;
	}


	public Date getRepairStart() {
		return repairStart;
	}


	public void setRepairStart(Date repairStart) {
		this.repairStart = repairStart;
	}


	public Date getRepairEnd() {
		return repairEnd;
	}


	public void setRepairEnd(Date repairEnd) {
		this.repairEnd = repairEnd;
	}
}
