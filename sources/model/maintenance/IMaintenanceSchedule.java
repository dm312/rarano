package model.maintenance;

import java.time.LocalDateTime;

public interface IMaintenanceSchedule {

	public LocalDateTime getStartTask();
	public void setStartTask(LocalDateTime start);

	public LocalDateTime setEndTask();
	public void setEndTask(LocalDateTime end);

}