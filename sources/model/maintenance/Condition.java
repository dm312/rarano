package model.maintenance;

public enum Condition {
	NEW (100), FINE (60), OLD (40), BROKEN(10);

	private final int use; 
	Condition(int use){
		this.use = use;
	}
	public int getUse() {
		return use;
	}
	
}
