package model.use;

import java.time.LocalDateTime;

import dal.FacilityDAO;

public class Use implements IFacility_Use {
	
	private int fee; 
	private Boolean vacant;
	private Inspections checks;
	private FacilityDAO bridge; 
	
	@Override
	public Object isInUseDuringInterval(LocalDateTime earliest,
			LocalDateTime latest) {
		return null;
	}

	@Override
	public Object assignFacilityToUse(int number) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object vacateFacility(int number) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object listInspections() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object listActualUsage() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object calcUsageRate(int number) {
		// TODO Auto-generated method stub
		return null;
	}
	
	//getters and setters
	public int getFee() {
		return fee;
	}
	public void setFee(int fee) {
		this.fee = fee;
	}
	public Boolean getVacant() {
		return vacant;
	}
	public void setVacant(Boolean vacant) {
		this.vacant = vacant;
	}
	public Inspections getChecks() {
		return checks;
	}
	public void setChecks(Inspections checks) {
		this.checks = checks;
	}
	public FacilityDAO getBridge() {
		return bridge;
	}
	public void setBridge(FacilityDAO bridge) {
		this.bridge = bridge;
	} 
}
