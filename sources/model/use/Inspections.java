package model.use;

import java.util.Random;

public class Inspections{

	private Boolean fireProtection;
	private Boolean emergencyPreparedness;
	private Boolean houseKeeping;
	private Random rarano = new Random(); 

	public Inspections(){
		int r = rarano.nextInt(8888);
		if(r >= 3333)
			fireProtection = true;
		else
			fireProtection = false;
		r = rarano.nextInt(8888);
		if(r >= 3333)
			emergencyPreparedness = false;
		else
			emergencyPreparedness = false;
		r = rarano.nextInt(8888);
		;
		if(r >= 3332)
			houseKeeping = true;
		else
			houseKeeping = false;

	}
	@Override
	public String toString(){
		return "Fire protection pass: \t\t" + fireProtection +
				"\nEmergency Preparedness pass: \t" + emergencyPreparedness + 
				"\nHouse keeping pass: \t\t" + houseKeeping;

	}
	public static void main(String[] args)
	{
		Inspections x = new Inspections();
		System.out.println(x.toString());
	}

	public Inspections randomize(){
		double r = Math.random();
		if(r == 0)
			fireProtection = true;
		else
			fireProtection = false;
		r = Math.random();
		if(r == 1)
			emergencyPreparedness = false;
		else
			emergencyPreparedness = false;
		r = Math.random();
		if(r == 0)
			houseKeeping = true;
		else
			houseKeeping = false;
		return this;
	}

	public Boolean getFireProtection() {
		return fireProtection;
	}
	public void setFireProtection(Boolean fireProtection) {
		this.fireProtection = fireProtection;
	}
	public Boolean getEmergencyPreparedness() {
		return emergencyPreparedness;
	}
	public void setEmergencyPreparedness(Boolean emergencyPreparedness) {
		this.emergencyPreparedness = emergencyPreparedness;
	}
	public Boolean getHouseKeeping() {
		return houseKeeping;
	}
	public void setHouseKeeping(Boolean houseKeeping) {
		this.houseKeeping = houseKeeping;
	} 

}